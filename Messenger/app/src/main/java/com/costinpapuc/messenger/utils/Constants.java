package com.costinpapuc.messenger.utils;

/**
 * Created by Jorj on 04-Apr-16.
 */
public class Constants {
    public static final int GROUP_LEAVE = 1;
    public static final int GROUP_CREATE = 2;
    public static final int GROUP_ADD_FRIENDS = 3;
    public static final int CHOOSE_FILE = 4;
    public static final String SERVER_ADDRESS =  "http://192.168.1.3:8080/";
    //public static final String SERVER_ADDRESS =  "http://10.0.3.2:8080/";
    //public static final String SERVER_ADDRESS =  "http://86.34.36.32:8080/";

    public static final String[] MESS = {" Hey there.",
            " ... Hi?",
            " ☺... (offers hand) My name is Cody and haha, you look like you could use someone cool to talk to.",
            " Oh? (is that what you think?) I am kind of bored. Sarah.",
            " Nice to meet you Sarah, I always liked that name.",
            " Me too. Haha probably because it’s mine.",
            " I’m the same way... So who are you here with?",
            " These guys (points) – the one in red is my roommate.",
            " So you are studying here... working? How is she, is she crazy or is she a cool girl?",
            " She is a little crazy but mostly cool. I’m studying language and communication.",
            " You ever catch her talking to herself?",
            " Haha! No, but she might have caught me. I’m a little quirky.",
            " All the best people are quirky... I bet I can outdo you though.",
            " Ha!",
            " Let’s see if you can keep up. (little contest follows and friend comes over)",
            "Friend: Oh hey, who are you?",
            " I just met him; we were talking about you a little before.",
            " Nothing serious, I was asking if you ever caught her talking to herself.",
            "Friend: Ha oh! Hey Sarah just wanted to come over before I go to the bar to see if you want something.",
            " What are you drinking?"};
}
