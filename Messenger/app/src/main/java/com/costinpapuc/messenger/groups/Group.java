package com.costinpapuc.messenger.groups;

import java.util.List;

/**
 * Created by Jorj on 09-Apr-16.
 */
public class Group {
    String id;
    String name;
    List<String> members;

    Group () {}

    public Group (String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

    @Override
    public String toString() {
        return id + " " + name;
    }
}

