package com.costinpapuc.messenger.chat;

/**
 * Created by Jorj on 04-Apr-16.
 */
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.costinpapuc.messenger.API.ServerAPI;
import com.costinpapuc.messenger.R;
import com.costinpapuc.messenger.utils.CircleTransformation;
import com.costinpapuc.messenger.utils.Constants;
import com.facebook.AccessToken;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MessageRecyclerViewAdapter extends RecyclerView.Adapter<MessageRecyclerViewAdapter.ViewHolder> {

    private List<Message> messageList;
    public String friendId;

    public MessageRecyclerViewAdapter(List<Message> items, String id) {
        messageList = items;
        Log.d("[LOG]", id);
        friendId = id;
    }

    public void addItem(final String message, final RecyclerView recyclerView) {

        Retrofit retrofit2 = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_ADDRESS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ServerAPI serverAPI2 = retrofit2.create(ServerAPI.class);
        Call<ResponseBody> call2 = serverAPI2.getTime();
        call2.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call2, Response<ResponseBody> response) {
                Long l = (long) 0;
                try {
                    l = Long.valueOf(response.body().string());
                    Log.d("[Retrofit]", "Successsssssssssssssssss " + l);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Message mes = new Message(AccessToken.getCurrentAccessToken().getUserId(),
                        friendId, l,
                        message);

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Constants.SERVER_ADDRESS)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                ServerAPI serverAPI = retrofit.create(ServerAPI.class);
                Call<Message> call = serverAPI.createMessage(
                        AccessToken.getCurrentAccessToken().getToken(),
                        mes);
                call.enqueue(new Callback<Message>() {
                    @Override
                    public void onResponse(Call<Message> call, Response<Message> response) {
                        Log.d("[Retrofit]", "Success");
                    }

                    @Override
                    public void onFailure(Call<Message> call, Throwable t) {
                        Log.d("[Retrofit]", "Error mes" + t.getMessage());
                    }
                });
                messageList.add(mes);
                notifyItemInserted(messageList.size() - 1);
                recyclerView.smoothScrollToPosition(getItemCount() - 1);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("[Retrofit]", "Error long" + t.getMessage());
            }
        });

    }

    public void addItems(List<Message> friends) {
        messageList.addAll(friends);
        notifyItemInserted(messageList.size() - friends.size());
        notifyItemRangeInserted(messageList.size() - friends.size(), messageList.size());
    }

    public Long getLastItemDate(){
        if (messageList == null)
            return (long)0;
        if (messageList.isEmpty())
            return (long)0;
        long l = messageList.get(messageList.size()-1).getDate();
        return l;
    }

    public void swap(List<Message> friends) {
        messageList.clear();
        messageList.addAll(friends);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_message, parent, false);
        return new ViewHolder(view, viewType);
    }

    @Override
    public int getItemViewType(int position) {
        return messageList.get(position).getSender().equals(AccessToken.getCurrentAccessToken().getUserId()) ? 1 : 0;
    }

    public boolean writeFile(ViewHolder holder, ResponseBody body, String filename){
        try {
            File f = Environment.getExternalStorageDirectory();
            File theFile = new File(f + File.separator + "Messenger/downloads/"
                    + friendId + File.separator + filename);
            File dir = theFile.getParentFile();
            if (!dir.exists())
                dir.mkdirs();

            InputStream inputStream = null;
            OutputStream outputStream = null;
            try {
                byte[] fileReader = new byte[4096];

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(theFile);

                while (true) {
                    int read = inputStream.read(fileReader);
                    if (read == -1)
                        break;
                    outputStream.write(fileReader, 0, read);
                }
                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.message = messageList.get(position);
        String profileImageUrl = Constants.SERVER_ADDRESS + "user/"
                + AccessToken.getCurrentAccessToken().getToken()
                + "/" + holder.message.getSender() + "/image";

        Log.d("[Picasso]", profileImageUrl);
        Picasso.with(holder.idView.getContext()).load(profileImageUrl).transform(new CircleTransformation()).into(holder.idView);
        holder.idView.setVisibility(View.VISIBLE);

        if (messageList.get(position).getContent().startsWith("file:///")) {
            final String filename = messageList.get(position).getContent().replace("file:///", "");
            if (getItemViewType(position) == 1)  {
                holder.messageView.setText(filename);
            }
            else {
                holder.messageView.setText("receiving:///" + filename);

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Constants.SERVER_ADDRESS)
                        .build();
                ServerAPI serverAPI = retrofit.create(ServerAPI.class);
                Call<ResponseBody> call = serverAPI.download(AccessToken.getCurrentAccessToken().getToken(),
                        friendId, filename);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                        Log.d("[Retrofit]", "File downloaded");
                        final boolean ok = writeFile(holder, response.body(), filename);
                        if (!ok)
                            Log.d("[Retrofit]", "Error write file");
                        holder.messageView.setText(filename);
                        holder.messageView.setLinksClickable(true);
                        holder.messageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String fname = ((TextView)v).getText().toString();
                                File f = Environment.getExternalStorageDirectory();
                                File theFile = new File(f + File.separator + "Messenger/downloads/"
                                        + friendId + File.separator + fname);
                                if (!theFile.exists())
                                    return;

                                MimeTypeMap map = MimeTypeMap.getSingleton();
                                String ext = MimeTypeMap.getFileExtensionFromUrl(theFile.getName());
                                String type = map.getMimeTypeFromExtension(ext);

                                if (type == null)
                                    type = "*/*";

                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                Uri data = Uri.fromFile(theFile);
                                intent.setDataAndType(data, type);
                                PackageManager packageManager = v.getContext().getPackageManager();
                                if (intent.resolveActivity(packageManager) != null) {
                                    v.getContext().startActivity(intent);
                                } else {
                                    Log.d("[CEVA]", "NO APP");
                                    Toast.makeText(v.getContext(), "No app available", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.d("[Retrofit]", "Error download file " + t.getMessage());
                    }
                });
            }
        }
        else
            holder.messageView.setText(messageList.get(position).getContent());

        holder.messageView.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView idView;
        public final TextView messageView;
        public Message message;

        public ViewHolder(View view, int type) {
            super(view);
            mView = view;
            if (type == 0) {
                idView = (ImageView) view.findViewById(R.id.id1);
                messageView = (TextView) view.findViewById(R.id.message_text1);
            }
            else {
                idView = (ImageView) view.findViewById(R.id.id2);
                messageView = (TextView) view.findViewById(R.id.message_text2);
            }
        }

        @Override
        public String toString() {
            return super.toString() + " '" + messageView.getText() + "'";
        }
    }
}

