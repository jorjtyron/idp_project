package com.costinpapuc.messenger.friends;

/**
 * Created by Jorj on 03-Apr-16.
 */
public class Friend {
    String id;
    String name;
    String photo;

    Friend () {}

    Friend (String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
