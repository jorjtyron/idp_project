package com.costinpapuc.messenger.groups;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.costinpapuc.messenger.API.ServerAPI;
import com.costinpapuc.messenger.R;
import com.costinpapuc.messenger.friends.Friend;
import com.costinpapuc.messenger.utils.Constants;
import com.facebook.AccessToken;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class CreateGroupFragment extends Fragment {

    private List<Friend> friends;
    RecyclerView recyclerView;
    CreateGroupRecyclerViewAdapter adapter;

    Button createButton;
    EditText groupName;

    public CreateGroupFragment() {
        friends = new ArrayList<>();


    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_group_list, container, false);

        final Context context = view.getContext();
        groupName = (EditText) view.findViewById(R.id.set_group_name);
        createButton = (Button) view.findViewById(R.id.create_group_button);

        final Intent intent = getActivity().getIntent();
        final int type = intent.getIntExtra("type", 0);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_ADDRESS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ServerAPI serverAPI= retrofit.create(ServerAPI.class);
        Call<List<Friend>> call = serverAPI.listFriends(AccessToken.getCurrentAccessToken().getToken());
        call.enqueue(new Callback<List<Friend>>() {
            @Override
            public void onResponse(Call<List<Friend>> call, Response<List<Friend>> response) {
                friends = response.body();
                if (friends != null) {
                    Log.d("[Retrofit]", "Success" + friends.toString());
                    if (type == Constants.GROUP_ADD_FRIENDS) {
                        List<String> memberIds = intent.getStringArrayListExtra("groupMembers");
                        for(Iterator<Friend> i = friends.iterator(); i.hasNext();) {
                            Friend f = i.next();
                            if (memberIds.contains(f.getId()))
                                i.remove();
                        }
                    }
                    adapter.swap(friends);
                }
                //Log.d("[Retrofit]", "Success" + friends.get(0).getPhoto());
            }

            @Override
            public void onFailure(Call<List<Friend>> call, Throwable t) {
                Log.d("[Retrofit]", "Error" + t.getMessage());
            }
        });

        if (type == Constants.GROUP_ADD_FRIENDS) {
            groupName.setVisibility(View.GONE);
            createButton.setText(getActivity().getString(R.string.add));
        }

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == Constants.GROUP_CREATE) {
                    if (groupName.getText().toString().equals("")) {
                        Toast.makeText(context, "Group Name can't be empty", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (adapter.getTheChosenOnes().size() < 2) {
                        Toast.makeText(context, "You must add at least two friends", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Intent resIntent = new Intent();
                    resIntent.putExtra("groupName", groupName.getText().toString());
                    ArrayList<String> members = new ArrayList<>(adapter.getTheChosenOnes());
                    members.add(AccessToken.getCurrentAccessToken().getUserId());
                    resIntent.putStringArrayListExtra("groupMembers", members);
                    getActivity().setResult(Activity.RESULT_OK, resIntent);
                }
                if (type == Constants.GROUP_ADD_FRIENDS) {
                    if (adapter.getTheChosenOnes().size() < 1) {
                        Toast.makeText(context, "You must add at least one friend", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Intent resIntent = new Intent();
                    ArrayList<String> members = new ArrayList<>(adapter.getTheChosenOnes());
                    members.addAll(intent.getStringArrayListExtra("groupMembers"));
                    resIntent.putExtra("groupId", intent.getStringExtra("groupId"));
                    resIntent.putStringArrayListExtra("groupMembers", members);
                    getActivity().setResult(Activity.RESULT_OK, resIntent);
                }
                getActivity().finish();
            }
        });
        recyclerView = (RecyclerView) view.findViewById(R.id.list_of_friends);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        adapter = new CreateGroupRecyclerViewAdapter(friends);
        recyclerView.setAdapter(adapter);

        return view;
    }

}
