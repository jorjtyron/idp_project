package com.costinpapuc.messenger.groups;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.costinpapuc.messenger.R;
import com.costinpapuc.messenger.friends.Friend;
import com.costinpapuc.messenger.utils.CircleTransformation;
import com.costinpapuc.messenger.utils.Constants;
import com.facebook.AccessToken;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;

public class CreateGroupRecyclerViewAdapter extends RecyclerView.Adapter<CreateGroupRecyclerViewAdapter.ViewHolder> {

    private List<Friend> friendList;
    private List<String> theChosenOnes;

    public CreateGroupRecyclerViewAdapter(List<Friend> items) {
        this.theChosenOnes = new ArrayList<>();
        if (items != null)
            friendList = items;
        else
            friendList = new ArrayList<>();
    }

    public List<String> getTheChosenOnes() {
        return theChosenOnes;
    }

    public void swap(List<Friend> friends){
        friendList.clear();
        friendList.addAll(friends);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_create_group, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.friend = friendList.get(position);
        holder.nameView.setText(friendList.get(position).getName());

        String profileImageUrl = Constants.SERVER_ADDRESS + "user/"
                + AccessToken.getCurrentAccessToken().getToken()
                + "/" + holder.friend.getId() + "/image";
        Log.d("[Picasso]", profileImageUrl);
        Picasso.with(holder.nameView.getContext()).load(profileImageUrl)
                .transform(new CircleTransformation())
                .error(R.drawable.error).into(holder.imageView);

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.checkBox.isChecked()) {
                    theChosenOnes.add(holder.friend.getId());
                }
                else {
                    theChosenOnes.remove(holder.friend.getId());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return friendList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView nameView;
        public final ImageView imageView;
        public final CheckBox checkBox;
        public Friend friend;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            nameView = (TextView) view.findViewById(R.id.name__);
            imageView = (ImageView) view.findViewById(R.id.image__);
            checkBox = (CheckBox) view.findViewById(R.id.checkbox__);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + nameView.getText() + "'";
        }
    }
}
