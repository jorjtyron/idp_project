package com.costinpapuc.messenger.groups;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.costinpapuc.messenger.API.ServerAPI;
import com.costinpapuc.messenger.R;
import com.costinpapuc.messenger.utils.Constants;
import com.facebook.AccessToken;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnGroupInteractionListener}
 * interface.
 */
public class GroupFragment extends Fragment {

    private OnGroupInteractionListener groupListener;
    private List<Group> groups;

    private GroupRecyclerViewAdapter adapter;
    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;

    public GroupRecyclerViewAdapter getAdapter() {
        return adapter;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public GroupFragment() {
        groups = new ArrayList<>();
//        groups.add(new Group("Group Name 1"));
//        groups.add(new Group("Group Name 2"));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_ADDRESS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ServerAPI serverAPI= retrofit.create(ServerAPI.class);
        Call<List<Group>> call = serverAPI.listGroups(AccessToken.getCurrentAccessToken().getToken());

        call.enqueue(new Callback<List<Group>>() {
            @Override
            public void onResponse(Call<List<Group>> call, Response<List<Group>> response) {
                groups = response.body();
                if (groups != null) {
                    Log.d("[Retrofit]", "Success" + groups.toString());
                    //adapter.addItems(groups);
                    adapter.swap(groups);
                    //recyclerView.smoothScrollToPosition(adapter.getItemCount());
                }
                //Log.d("[Retrofit]", "Success" + friends.get(0).getPhoto());
            }

            @Override
            public void onFailure(Call<List<Group>> call, Throwable t) {
                Log.d("[Retrofit]", "Error" + t.getMessage());
            }
        });

    }

    public static GroupFragment newInstance() {
        return new GroupFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_group_list, container, false);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("groupList")) {
                Gson gson = new Gson();
                groups = gson.fromJson(savedInstanceState.getString("groupList"), new TypeToken<Collection<Group>>(){}.getType());
            }
        }
        else {
            SharedPreferences pr = getActivity().getPreferences(Context.MODE_PRIVATE);
            String s = pr.getString("groupList", "default");
            if (!s.equals("default")) {
                Gson gson = new Gson();
                groups = gson.fromJson(s, new TypeToken<Collection<Group>>(){}.getType());
            }
        }

        // Set the adapter
        Context context = view.getContext();
        recyclerView = (RecyclerView) view.findViewById(R.id.group_list);
        recyclerView.setLayoutManager(new GridLayoutManager(context, 2));
        adapter = new GroupRecyclerViewAdapter(groups, groupListener);
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_group_list);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Constants.SERVER_ADDRESS)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                ServerAPI serverAPI= retrofit.create(ServerAPI.class);
                Call<List<Group>> call = serverAPI.listGroups(AccessToken.getCurrentAccessToken().getToken());

                call.enqueue(new Callback<List<Group>>() {
                    @Override
                    public void onResponse(Call<List<Group>> call, Response<List<Group>> response) {
                        groups = response.body();
                        if (groups != null) {
                            Log.d("[Retrofit]", "Success" + groups.toString());
                            adapter.swap(groups);
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }
                    @Override
                    public void onFailure(Call<List<Group>> call, Throwable t) {Log.d("[Retrofit]", "Error" + t.getMessage());}
                });
            }
        });
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnGroupInteractionListener) {
            groupListener = (OnGroupInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnGroupInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        groupListener = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Gson gson = new Gson();
        String s = gson.toJson(adapter.getGroupList());
        outState.putString("groupList", s);
        SharedPreferences pr = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pr.edit();
        editor.putString("groupList", s);
        editor.apply();
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("groupList")) {
                Gson gson = new Gson();
                groups = gson.fromJson(savedInstanceState.getString("groupList"), new TypeToken<Collection<Group>>(){}.getType());
            }
        }
        super.onViewStateRestored(savedInstanceState);
    }

    public interface OnGroupInteractionListener {
        void onGroupInteraction(Group group);
        void onGroupMenuInteraction(Group group, int type);
    }
}
