package com.costinpapuc.messenger.chat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.costinpapuc.messenger.API.ServerAPI;
import com.costinpapuc.messenger.R;
import com.costinpapuc.messenger.groups.GroupFragment;
import com.costinpapuc.messenger.utils.Constants;
import com.facebook.AccessToken;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ChatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Toolbar toolbar = (Toolbar) findViewById(R.id.chat_toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        setTitle(intent.getStringExtra("Name"));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
