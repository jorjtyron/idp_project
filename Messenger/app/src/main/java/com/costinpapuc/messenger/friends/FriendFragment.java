package com.costinpapuc.messenger.friends;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.costinpapuc.messenger.API.ServerAPI;
import com.costinpapuc.messenger.R;
import com.costinpapuc.messenger.groups.Group;
import com.costinpapuc.messenger.utils.Constants;
import com.facebook.AccessToken;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFriendInteractionListener}
 * interface.
 */
public class FriendFragment extends Fragment {

    private OnFriendInteractionListener friendListener;
    private List<Friend> friends;
    RecyclerView recyclerView;
    FriendRecyclerViewAdapter adapter;

    public FriendFragment() {
        friends = new ArrayList<>();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_ADDRESS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ServerAPI serverAPI= retrofit.create(ServerAPI.class);
        Call<List<Friend>> call = serverAPI.listFriends(AccessToken.getCurrentAccessToken().getToken());

        call.enqueue(new Callback<List<Friend>>() {
            @Override
            public void onResponse(Call<List<Friend>> call, Response<List<Friend>> response) {
                friends = response.body();
                if (friends != null) {
                    Log.d("[Retrofit]", "Success" + friends.toString());
                    if (adapter != null)
                        adapter.swap(friends);
                }
                //Log.d("[Retrofit]", "Success" + friends.get(0).getPhoto());
            }

            @Override
            public void onFailure(Call<List<Friend>> call, Throwable t) {
                Log.d("[Retrofit]", "Error" + t.getMessage());
            }
        });
    }

    public static FriendFragment newInstance() {
        return new FriendFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friend_list, container, false);

        if (savedInstanceState != null) {
            //Log.d("[Gson]", "Nu e null in Oncreate");
            if (savedInstanceState.containsKey("friendList")) {
                Gson gson = new Gson();
                friends = gson.fromJson(savedInstanceState.getString("friendList"), new TypeToken<Collection<Friend>>(){}.getType());
            }
        }
        else {
            SharedPreferences pr = getActivity().getPreferences(Context.MODE_PRIVATE);
            String s = pr.getString("friendList", "default");
            //Log.d("[LOG]", "onCreate Friend Fragment");
            if (!s.equals("default")) {
                //Log.d("[LOG]", "onCreate Friend Fragment not default");
                Gson gson = new Gson();
                friends = gson.fromJson(s, new TypeToken<Collection<Friend>>(){}.getType());
                //Log.d("[LOG]", "onCreate Friend" + friends);
            }
        }

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));

            adapter = new FriendRecyclerViewAdapter(friends, friendListener);
            recyclerView.setAdapter(adapter);
        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFriendInteractionListener) {
            friendListener = (OnFriendInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFriendInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        friendListener = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Gson gson = new Gson();
        String s = gson.toJson(adapter.getFriendList());
        outState.putString("friendList", s);
        SharedPreferences pr = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pr.edit();
        editor.putString("friendList", s);
        editor.apply();
        //Log.d("[Gson]", "onSaveInstanceState" + s);
    }



    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            //Log.d("[Gson]", "Not null on view state restore");
            if (savedInstanceState.containsKey("friendList")) {
                Gson gson = new Gson();
                friends = gson.fromJson(savedInstanceState.getString("friendList"), new TypeToken<Collection<Friend>>(){}.getType());
            }
        }
        //Log.d("[Gson]", "Null in onviewStateResore");
        super.onViewStateRestored(savedInstanceState);
    }

    public interface OnFriendInteractionListener {
        void onFriendInteraction(Friend item);
    }
}
