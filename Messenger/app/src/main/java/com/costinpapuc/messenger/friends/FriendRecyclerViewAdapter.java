package com.costinpapuc.messenger.friends;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.costinpapuc.messenger.R;
import com.costinpapuc.messenger.friends.FriendFragment.OnFriendInteractionListener;
import com.costinpapuc.messenger.utils.CircleTransformation;
import com.costinpapuc.messenger.utils.Constants;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.internal.ImageRequest;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FriendRecyclerViewAdapter extends RecyclerView.Adapter<FriendRecyclerViewAdapter.ViewHolder> {

    private final List<Friend> friendList;
    private final OnFriendInteractionListener friendListener;

    public FriendRecyclerViewAdapter(List<Friend> items, OnFriendInteractionListener listener) {
        if (items != null)
            friendList = items;
        else
            friendList = new ArrayList<>();
        friendListener = listener;
    }

    public void swap(List<Friend> friends){
        friendList.clear();
        friendList.addAll(friends);
        notifyDataSetChanged();
    }

    public List<Friend> getFriendList() {
        return friendList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_friend, parent, false);
        return new ViewHolder(view);
    }

    //Target target;

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.friend = friendList.get(position);
        //byte[] byteArray = friendList.get(position).photo.getBytes();
        //Bitmap photo = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        //holder.photoView.setImageBitmap(photo);
        holder.nameView.setText(friendList.get(position).name);

        String profileImageUrl = Constants.SERVER_ADDRESS + "user/"
                + AccessToken.getCurrentAccessToken().getToken()
                + "/" + holder.friend.getId() + "/image";
        Log.d("[Picasso]", profileImageUrl);
        Picasso.with(holder.nameView.getContext()).load(profileImageUrl)
                .transform(new CircleTransformation())
                .placeholder(R.drawable.image)
                .error(R.drawable.error).into(holder.imageView);

//      target = new Target() {
//            @Override
//            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                Drawable d = new BitmapDrawable(holder.nameView.getContext().getResources(), bitmap);
//                holder.nameView.setCompoundDrawablesWithIntrinsicBounds(d, null, null, null);
//            }
//            @Override
//            public void onBitmapFailed(Drawable errorDrawable) {}
//
//            @Override
//            public void onPrepareLoad(Drawable placeHolderDrawable) {}
//        };
//
//        GraphRequest request = GraphRequest.newMeRequest(
//                AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
//                    @Override
//                    public void onCompleted(JSONObject me, GraphResponse response) {
//                        if (AccessToken.getCurrentAccessToken() != null) {
//                            if (me != null) {
//                                String profileImageUrl = ImageRequest.getProfilePictureUri(me.optString(holder.friend.getId()), 60, 60).toString();
//                                Log.i("[Facebook]", profileImageUrl);
//                                Picasso.with(holder.nameView.getContext()).load(profileImageUrl).into(target);
//
//                            }
//                        }
//                    }
//                });
//        GraphRequest.executeBatchAsync(request);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != friendListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    friendListener.onFriendInteraction(holder.friend);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return friendList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView nameView;
        public final ImageView imageView;
        public Friend friend;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            nameView = (TextView) view.findViewById(R.id.name);
            imageView = (ImageView) view.findViewById(R.id.image_);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + nameView.getText() + "'";
        }
    }
}
