package com.costinpapuc.messenger.chat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.costinpapuc.messenger.API.ServerAPI;
import com.costinpapuc.messenger.R;
import com.costinpapuc.messenger.utils.Constants;
import com.facebook.AccessToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A placeholder fragment containing a simple view.
 */

public class MessageFragment extends Fragment implements View.OnClickListener {

    private RecyclerView recyclerView;
    private List<Message> messageList;

    private EditText messageEditText;
    private ImageButton messageSendButton;
    private ImageButton fileSendButton;


    private MessageRecyclerViewAdapter adapter;

    AsyncTask asyncTask;

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.message_send_button) {
            if (messageEditText.getText().toString().equals(""))
                return;
            adapter.addItem(messageEditText.getText().toString(), recyclerView);
            messageEditText.setText("");
            //recyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
        }
        if (v.getId() == R.id.file_send_button) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT, null);
            intent.setType("*/*");
            startActivityForResult(intent, Constants.CHOOSE_FILE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_CANCELED) {
            Log.d("[ACT_RES]", "canceled" + resultCode + " " + requestCode);
            return;
        }
        if (requestCode == Constants.CHOOSE_FILE) {
            Uri uri = data.getData();
            Log.d("[ACT_RES]", "data " + uri.getPath());

            final File file = new File(uri.getPath());

            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part body =
                    MultipartBody.Part.createFormData("file", file.getName(), requestFile);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.SERVER_ADDRESS)
                    .build();
            ServerAPI serverAPI = retrofit.create(ServerAPI.class);
            Call<Void> call = serverAPI.upload(AccessToken.getCurrentAccessToken().getToken(),
                    adapter.friendId, body);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    Log.d("[Retrofit]", "File uploaded");
                    adapter.addItem("file:///" + file.getName(), recyclerView);
                }
                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Log.d("[Retrofit]", "Error upload file " + t.getMessage());
                }
            });
            return;
        }
    }

    public MessageFragment() {
        messageList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message_list, container, false);

        messageEditText = (EditText) view.findViewById(R.id.message_edit_text);
        messageSendButton = (ImageButton) view.findViewById(R.id.message_send_button);
        messageSendButton.setOnClickListener(this);
        fileSendButton = (ImageButton) view.findViewById(R.id.file_send_button);
        fileSendButton.setOnClickListener(this);
        //if (view instanceof RecyclerView) {
        Context context = view.getContext();
        recyclerView = (RecyclerView)view.findViewById(R.id.message_list);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        //mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(mLayoutManager);

        messageList = new ArrayList<>();

        final String id = getActivity().getIntent().getExtras().getString("Id");
        final String type = getActivity().getIntent().getExtras().getString("type");

        if (type.equals("friendChat")){
            for (int i = 0; i < Constants.MESS.length; i++) {
                if (i % 2 == 0)
                    messageList.add(new Message(id,
                            AccessToken.getCurrentAccessToken().getUserId(),
                            (long)i, Constants.MESS[i]));
                else
                    messageList.add(new Message(AccessToken.getCurrentAccessToken().getUserId(),
                            id,
                            (long)i, Constants.MESS[i]));
            }
        }

        adapter = new MessageRecyclerViewAdapter(messageList, id);
        recyclerView.setAdapter(adapter);

        asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
            while (true) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Constants.SERVER_ADDRESS)
                        .addConverterFactory(GsonConverterFactory.create())
                        //.addConverterFactory(JacksonConverterFactory.create())
                        .build();
                ServerAPI serverAPI = retrofit.create(ServerAPI.class);
                Call<List<Message>> call = serverAPI.listMessages(
                        AccessToken.getCurrentAccessToken().getToken(),
                        id,
                        adapter.getLastItemDate().toString(),
                        type);
                //Log.d("[Retrofit]", "LAST" + adapter.getLastItemDate().toString());
                call.enqueue(new Callback<List<Message>>() {
                    @Override
                    public void onResponse(Call<List<Message>> call, Response<List<Message>> response) {
                        messageList = response.body();
                        if (messageList != null) {
                            //Log.d("[Retrofit]", "Success" + messageList.toString());
                            if (!messageList.isEmpty()) {
                                adapter.addItems(messageList);
                                recyclerView.smoothScrollToPosition(adapter.getItemCount());
                            }
                        }
                        //Log.d("[Retrofit]", "Success" + friends.get(0).getPhoto());
                    }

                    @Override
                    public void onFailure(Call<List<Message>> call, Throwable t) {
                        //Log.d("[Retrofit]", "Error" + t.getMessage());
                    }
                });
                if(this.isCancelled())
                    break;
                try {
                    Thread.sleep(2000);
                } catch (Exception e) {
                    //e.printStackTrace();
                }
                if(this.isCancelled())
                    break;
            }
            return null;
            }
        };
        asyncTask.execute(null);
        return view;
    }

    @Override
    public void onDestroyView() {
        Log.d("[CEVA]", "onDestroyView");
        asyncTask.cancel(true);
        super.onDestroyView();
    }
}
