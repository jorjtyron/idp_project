package com.costinpapuc.messenger;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.Toast;

import com.costinpapuc.messenger.API.ServerAPI;
import com.costinpapuc.messenger.chat.ChatActivity;
import com.costinpapuc.messenger.friends.Friend;
import com.costinpapuc.messenger.friends.FriendFragment;
import com.costinpapuc.messenger.groups.CreateGroupActivity;
import com.costinpapuc.messenger.groups.Group;
import com.costinpapuc.messenger.groups.GroupFragment;
import com.costinpapuc.messenger.utils.Constants;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MessengerActivity extends AppCompatActivity implements FriendFragment.OnFriendInteractionListener, GroupFragment.OnGroupInteractionListener {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messenger);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }

    @Override
    protected void onDestroy() {
        Log.d("[LOG]", "onDestroy messenger activity");
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_messenger, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Log.d("[MYTAG]", "Enter MENU " + id + " " + R.id.home + " " + R.id.up);
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Log.d("[MYTAG]", "Enter Settings");
            return true;
        }

        if (id == android.R.id.home) {
            Log.d("[MYTAG]", "Enter Up");
            LoginManager.getInstance().logOut();
        }
        Log.d("[MYTAG]", "Exit MENU");
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFriendInteraction(Friend friend) {
        Toast.makeText(this, "Clicked" + friend.getName(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("type", "friendChat");
        intent.putExtra("Id", friend.getId());
        intent.putExtra("Name", friend.getName());
        startActivity(intent);
    }

    @Override
    public void onGroupInteraction(Group group) {
        if (group == null) {
            Intent intent = new Intent(this, CreateGroupActivity.class);
            intent.putExtra("type", Constants.GROUP_CREATE);
            startActivityForResult(intent, Constants.GROUP_CREATE);
            return;
        }
        Toast.makeText(this, "Clicked" + group.getName(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("type", "groupChat");
        intent.putExtra("Id", group.getId());
        intent.putExtra("Name", group.getName());
        startActivity(intent);
    }

    @Override
    public void onGroupMenuInteraction(Group group, int type) {
        if (type == Constants.GROUP_ADD_FRIENDS) {
            Intent intent = new Intent(this, CreateGroupActivity.class);
            intent.putExtra("type", type);
            intent.putStringArrayListExtra("groupMembers", new ArrayList<>(group.getMembers()));
            intent.putExtra("groupId", group.getId());
            startActivityForResult(intent, Constants.GROUP_ADD_FRIENDS);
        }
        if (type == Constants.GROUP_LEAVE) {
            Log.d("[ceva]", group.getId());
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.SERVER_ADDRESS)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            ServerAPI serverAPI = retrofit.create(ServerAPI.class);
            Call<Void> call = serverAPI.leaveGroup(
                    AccessToken.getCurrentAccessToken().getToken(),
                    group.getId());
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    Log.d("[Retrofit]", "Success leave group");
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Log.d("[Retrofit]", "Error Success leave group " + t.getMessage());
                }
            });
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_CANCELED) {
            Log.d("[ACT_RES]", "canceled" + resultCode);
            return;
        }

        if (requestCode == Constants.GROUP_CREATE) {
            final Group group = new Group(data.getStringExtra("groupName"));
            group.setMembers(data.getStringArrayListExtra("groupMembers"));
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.SERVER_ADDRESS)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            ServerAPI serverAPI = retrofit.create(ServerAPI.class);
            Call<Group> call = serverAPI.createGroup(
                    AccessToken.getCurrentAccessToken().getToken(),
                    group);
            call.enqueue(new Callback<Group>() {
                @Override
                public void onResponse(Call<Group> call, Response<Group> response) {
                    Log.d("[Retrofit]", "Success create group");
                    Group g = response.body();
                    Fragment page = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.container + ":" + mViewPager.getCurrentItem());
                    if (mViewPager.getCurrentItem() == 1 && page != null) {
                        ((GroupFragment)page).getAdapter().add(g);
                    }
                    //GroupFragment gf = (GroupFragment) mSectionsPagerAdapter.getItem(1);
                    //gf.getAdapter().add(group);
                }

                @Override
                public void onFailure(Call<Group> call, Throwable t) {
                    Log.d("[Retrofit]", "Error create group " + t.getMessage());
                }
            });
            return;
        }
        if (requestCode == Constants.GROUP_ADD_FRIENDS) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.SERVER_ADDRESS)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            ServerAPI serverAPI = retrofit.create(ServerAPI.class);
            Call<Void> call = serverAPI.updateGroup(
                    AccessToken.getCurrentAccessToken().getToken(),
                    data.getStringExtra("groupId"),
                    data.getStringArrayListExtra("groupMembers"));
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    Log.d("[Retrofit]", "Success update group");
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Log.d("[Retrofit]", "Error update group" + t.getMessage());
                }
            });
            return;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return FriendFragment.newInstance();
                case 1:
                    return GroupFragment.newInstance();
                case 2:
                    return FriendFragment.newInstance();
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Friends";
                case 1:
                    return "Groups";
                case 2:
                    return "Recent";
            }
            return null;
        }
    }
}
