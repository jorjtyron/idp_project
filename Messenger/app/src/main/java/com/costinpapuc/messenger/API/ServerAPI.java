package com.costinpapuc.messenger.API;


import com.costinpapuc.messenger.chat.Message;
import com.costinpapuc.messenger.friends.Friend;
import com.costinpapuc.messenger.groups.Group;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Jorj on 03-Apr-16.
 */
public interface ServerAPI {
    @GET("user/{api_token}/friends")
    Call<List<Friend>> listFriends(@Path("api_token") String api_token);

    @GET("time")
    Call<ResponseBody> getTime();

    @GET("user/{api_token}/groups")
    Call<List<Group>> listGroups(@Path("api_token") String api_token);

    @POST("user/{api_token}/message")
    Call<Message> createMessage(@Path("api_token") String api_token,
                                @Body Message message);

    @POST("user/{api_token}/group")
    Call<Group> createGroup(@Path("api_token") String api_token,
                            @Body Group group);

    @GET("user/{api_token}/{group_id}/leave")
    Call<Void> leaveGroup(@Path("api_token") String api_token,
                          @Path("group_id") String group_id);

    @POST("user/{api_token}/{group_id}/addfriends")
    Call<Void> updateGroup(@Path("api_token") String api_token,
                           @Path("group_id") String group_id,
                           @Body List<String> membersIds);


    @GET("/user/{api_token}/{friend_id}/messages/{date}/{type}")
    Call<List<Message>> listMessages(@Path("api_token") String api_token,
                                     @Path("friend_id") String friend_id,
                                     @Path("date") String date,
                                     @Path("type") String type);

    @Multipart
    @POST("/user/{api_token}/{friend_id}/file")
    Call<Void> upload(@Path("api_token") String api_token,
                      @Path("friend_id") String friend_id,
                      @Part MultipartBody.Part file);

    @GET("/user/{api_token}/{friend_id}/file/{filename}")
    Call<ResponseBody> download(@Path("api_token") String api_token,
                        @Path("friend_id") String friend_id,
                        @Path("filename") String filename);
}
