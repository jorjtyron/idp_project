package com.costinpapuc.messenger.groups;

import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.costinpapuc.messenger.R;
import com.costinpapuc.messenger.friends.Friend;
import com.costinpapuc.messenger.groups.GroupFragment.OnGroupInteractionListener;
import com.costinpapuc.messenger.utils.Constants;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Group} and makes a call to the
 * specified {@link OnGroupInteractionListener}.
 */
public class GroupRecyclerViewAdapter extends RecyclerView.Adapter<GroupRecyclerViewAdapter.ViewHolder> {

    private final List<Group> groupList;
    private final OnGroupInteractionListener groupListener;

    public GroupRecyclerViewAdapter(List<Group> items, OnGroupInteractionListener listener) {
        groupList = items;
        groupListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_group, parent, false);
        return new ViewHolder(view);
    }

    public void removeAt(int position) {
        groupList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, groupList.size());
    }

    public void add(Group group) {
        groupList.add(group);
        notifyItemInserted(groupList.size()-1);
    }

    public void addItems(List<Group> groups) {
        groupList.addAll(groups);
        notifyItemInserted(groupList.size() - groups.size());
        notifyItemRangeInserted(groupList.size() - groups.size(), groupList.size());
    }

    public void swap(List<Group> groups){
        groupList.clear();
        groupList.addAll(groups);
        notifyDataSetChanged();
    }

    public List<Group> getGroupList() {
        return groupList;
    }

    private void showPopupMenu(final View view) {

        // Retrieve the clicked item from view's tag
        final Integer item = (Integer) view.getTag();

        // Create a PopupMenu, giving it the clicked view for an anchor
        final PopupMenu popup = new PopupMenu(view.getContext(), view);

        // Inflate our menu resource into the PopupMenu's Menu
        popup.getMenuInflater().inflate(R.menu.popup, popup.getMenu());

        // Set a listener so we are notified if a menu item is clicked
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.menu_add:
                        Log.d("[ITEM]", "ADD");
                        groupListener.onGroupMenuInteraction(groupList.get(item), Constants.GROUP_ADD_FRIENDS);
                        return true;
                    case R.id.menu_leave:
                        Log.d("[ITEM]", "LEAVE");
                        new AlertDialog.Builder(view.getContext())
                                .setTitle("Leave group")
                                .setMessage("Are you sure you want to leave this group?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        groupListener.onGroupMenuInteraction(groupList.get(item), Constants.GROUP_LEAVE);
                                        removeAt(item);
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {}})
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                        return true;
                }
                return false;
            }
        });

        popup.show();
    }

    @Override
    public int getItemViewType(int position) {
        return groupList.size() == position ? 1 : 0;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (position == groupList.size()) {
            holder.popupButton.setVisibility(View.INVISIBLE);
            holder.nameView.setText("Add New Group");
            holder.nameView.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(holder.mView.getContext(), R.drawable.groupadd48),null,null);
        } else {
            holder.mItem = groupList.get(position);
            holder.nameView.setText(groupList.get(position).name);

            holder.popupButton.setTag(position);
            holder.popupButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    v.post(new Runnable() {
                        @Override
                        public void run() {
                            showPopupMenu(v);
                        }
                    });
                }
            });
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != groupListener) {
                    groupListener.onGroupInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return groupList.size() + 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView nameView;
        public final ImageView popupButton;
        public Group mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            popupButton = (ImageView) view.findViewById(R.id.button_popup);
            nameView = (TextView) view.findViewById(R.id.group_name);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + nameView.getText() + "'";
        }
    }
}
