package com.costinpapuc.messenger.chat;

/**
 * Created by Jorj on 04-Apr-16.
 */
public class Message {

    public String id;
    public String sender;
    public String receiver;
    public Long date;
    public String content;

    public Message(String sender, String receiver, Long date, String content) {
        //this.id = id;
        this.sender = sender;
        this.receiver = receiver;
        this.date = date;
        this.content = content;
    }

    public String getSender() {
        return sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public Long getDate() {
        return date;
    }

    public String getContent() {
        return content;
    }
}
