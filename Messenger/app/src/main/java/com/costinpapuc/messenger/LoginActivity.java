package com.costinpapuc.messenger;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.ArraySet;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.Arrays;

public class LoginActivity extends AppCompatActivity {
    private TextView infoTextView;
    private LoginButton loginButton;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_login);

        ShimmerFrameLayout shimmerFrameLayout =
                (ShimmerFrameLayout) findViewById(R.id.shimmer_view_container);
        shimmerFrameLayout.setDuration(5000);
        shimmerFrameLayout.startShimmerAnimation();

        infoTextView = (TextView)findViewById(R.id.info_text_view);
        loginButton = (LoginButton)findViewById(R.id.login_button);
        String[] permissions = {"user_friends", "public_profile"};
        loginButton.setReadPermissions(Arrays.asList(permissions));

        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        if (accessToken != null) {
            Intent intent = new Intent(LoginActivity.this, MessengerActivity.class);
            //intent.putExtra("Login", type);
            startActivity(intent);
            finish();
        }

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
//                infoTextView.setText(String.format("User ID: %s\nAuth Token: %s",
//                        loginResult.getAccessToken().getUserId(),
//                        loginResult.getAccessToken().getToken()));
                Intent intent = new Intent(LoginActivity.this, MessengerActivity.class);
                //intent.putExtra("Login", type);
                startActivity(intent);
            }

            @Override
            public void onCancel() {
                infoTextView.setText("Login attempt canceled.");
            }

            @Override
            public void onError(FacebookException e) {
                infoTextView.setText("Login attempt failed.");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }
}
