package com.costinpapuc;

/**
 * Created by Jorj on 04-Apr-16.
 */

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GroupRepository extends MongoRepository<Group, String> {

    public Group findByName(String name);
    public Group findById(String Id);
    public List<Group> findByMembersIn(String member);

    //public List<User> findByLastName(String name);

}
