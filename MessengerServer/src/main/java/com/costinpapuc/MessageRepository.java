package com.costinpapuc;

import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;

/**
 * Created by Jorj on 14-Apr-16.
 */
public interface MessageRepository extends MongoRepository<Message, String> {
    public List<Message> findBySenderAndReceiverAndDateGreaterThan(String sender, String receiver, Long date);
    public List<Message> findByReceiverAndDateGreaterThan(String receiver, Long date);

}