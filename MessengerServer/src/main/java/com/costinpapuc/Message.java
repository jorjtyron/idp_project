package com.costinpapuc;

import org.springframework.data.annotation.Id;

/**
 * Created by Jorj on 14-Apr-16.
 */

public class Message {

    @Id
    private String id;

    private Long date;
    private String sender;
    private String receiver;
    private String content;

    public Message() {
    }

    public Message(String sender, String receiver, Long date, String content) {
        this.date = date;
        this.sender = sender;
        this.content = content;
        this.receiver = receiver;
    }

    public Long getDate() {
        return date;
    }

    public String getSender() {
        return sender;
    }

    public String getContent() {
        return content;
    }

    public String getReceiver() {
        return receiver;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}


