package com.costinpapuc;

/**
 * Created by Jorj on 02-Apr-16.
 */
public class Friend {

    private final String id;
    private final String name;

    public Friend(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}

