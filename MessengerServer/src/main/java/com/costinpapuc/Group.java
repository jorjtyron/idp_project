package com.costinpapuc;

/**
 * Created by Jorj on 04-Apr-16.
 */
import org.springframework.data.annotation.Id;

import java.util.List;


public class Group {

    @Id
    private String id;

    private String name;
    private List<String> members;

    public Group() {}

    public Group(String name, List<String> members) {
        this.name = name;
        this.members = members;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }
}
