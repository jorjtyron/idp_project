package com.costinpapuc;

import com.sun.xml.internal.ws.api.policy.PolicyResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.ImageType;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@RestController
public class MessengerController {
    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private GroupRepository groupRepository;

    private static final String APP_ID = "915415388584293";
    private static final String APP_SECRET = "ec6d29613fabddc8e330b01be2de49f9";

    @RequestMapping("/user/{api_token}/friends")
    public List<Friend> getFriends(@PathVariable("api_token") String api_token) {

        System.out.println("Token: " + api_token);

        OAuth2Operations oauth = new FacebookConnectionFactory(APP_ID, APP_SECRET).getOAuthOperations();
        String accesToken = oauth.authenticateClient().getAccessToken();
        Facebook facebook = new FacebookTemplate(api_token);
        //User fbUser = facebook.userOperations().getUserProfile();
        List<String> friendIds = facebook.friendOperations().getFriendIds();

        List<Friend> friendList = new ArrayList<>();

        //System.out.println("Token: " + api_token);
        for (String id : friendIds) {
            //System.out.println("Am ajuns la prieteni");
            friendList.add(new Friend(id, facebook.userOperations().getUserProfile(id).getName()));
        }
//        friendList.add(new Friend("2", "gigel"));
//        friendList.add(new Friend("3", "Zgartzolog"));

        return friendList;
    }

    @RequestMapping(value = "/user/{api_token}/groups")
    public List<Group> getGroups(@PathVariable("api_token") String api_token)  {
        OAuth2Operations oauth = new FacebookConnectionFactory(APP_ID, APP_SECRET).getOAuthOperations();
        String accesToken = oauth.authenticateClient().getAccessToken();
        Facebook facebook = new FacebookTemplate(api_token);
        String user_id = facebook.userOperations().getUserProfile().getId();

        return groupRepository.findByMembersIn(user_id);
    }


    @RequestMapping("/time")
    public Long getFriends() {
        return System.currentTimeMillis();
    }

    @RequestMapping(value = "/user/{api_token}/{friend_id}/image", produces = "image/bmp")
    public byte[] getFile(@PathVariable("api_token") String api_token, @PathVariable("friend_id") String friend_id)  {
        OAuth2Operations oauth = new FacebookConnectionFactory(APP_ID, APP_SECRET).getOAuthOperations();
        String accesToken = oauth.authenticateClient().getAccessToken();
        Facebook facebook = new FacebookTemplate(api_token);

        //System.out.println("Friend ID " + friend_id);
        //System.out.println("User   ID " + facebook.userOperations().getUserProfile().getId());
        //System.out.println("get photo");
        return facebook.userOperations().getUserProfileImage(friend_id, ImageType.SQUARE);
    }

    @RequestMapping(value = "/user/{api_token}/{friend_id}/messages/{date}/{type}")
    public List<Message> getMessages(@PathVariable("api_token") String api_token,
                                     @PathVariable("friend_id") String friend_id,
                                     @PathVariable("date") String date,
                                     @PathVariable("type") String type) {

        OAuth2Operations oauth = new FacebookConnectionFactory(APP_ID, APP_SECRET).getOAuthOperations();
        String accesToken = oauth.authenticateClient().getAccessToken();
        Facebook facebook = new FacebookTemplate(api_token);
        String user_id = facebook.userOperations().getUserProfile().getId();

        System.out.println(api_token + "/" + friend_id);

        if (type.equals("friendChat")) {
            List<Message> him = messageRepository.findBySenderAndReceiverAndDateGreaterThan(friend_id, user_id, Long.parseLong(date));
            List<Message> mine = messageRepository.findBySenderAndReceiverAndDateGreaterThan(user_id, friend_id, Long.parseLong(date));
            him.addAll(mine);
            Collections.sort(him, new Comparator<Message>() {
                @Override
                public int compare(Message o1, Message o2) {
                    return o1.getDate().compareTo(o2.getDate());
                }
            });
            for (Message m: him) {
                System.out.print(m.getContent());
            }
            return him;
        }

        if(type.equals("groupChat")) {
            if(!groupRepository.findById(friend_id).getMembers().contains(user_id))
                return null;

            List<Message> messages = messageRepository.findByReceiverAndDateGreaterThan(friend_id, Long.parseLong(date));
            Collections.sort(messages, new Comparator<Message>() {
                @Override
                public int compare(Message o1, Message o2) {
                    return o1.getDate().compareTo(o2.getDate());
                }
            });
            for (Message m : messages) {
                System.out.print(m.getContent());
            }
            return messages;
        }
        return null;

    }


    @RequestMapping(value = "/user/{api_token}/message", method = RequestMethod.POST)
    public Message setMessage(@PathVariable("api_token") String api_token, @RequestBody Message message)  {
        System.out.println("Post Method");
        OAuth2Operations oauth = new FacebookConnectionFactory(APP_ID, APP_SECRET).getOAuthOperations();
        String accesToken = oauth.authenticateClient().getAccessToken();
        Facebook facebook = new FacebookTemplate(api_token);
        String user_id = facebook.userOperations().getUserProfile().getId();
        if (user_id.equals(message.getSender()))
            messageRepository.save(message);
        return message;
    }

    @RequestMapping(value = "/user/{api_token}/group", method = RequestMethod.POST)
    public Group createGroup(@PathVariable("api_token") String api_token, @RequestBody Group group)  {
        System.out.println("Post create group " + group.getId() + group.getName());
        for (String id : group.getMembers()) {
            System.out.println(id);
        }
        OAuth2Operations oauth = new FacebookConnectionFactory(APP_ID, APP_SECRET).getOAuthOperations();
        String accesToken = oauth.authenticateClient().getAccessToken();
        Facebook facebook = new FacebookTemplate(api_token);
        String user_id = facebook.userOperations().getUserProfile().getId();
        if (group.getMembers().contains(user_id))
            groupRepository.save(group);
        return group;
    }

    @RequestMapping(value = "/user/{api_token}/{group_id}/addfriends", method = RequestMethod.POST)
    public Void updateGroup(@PathVariable("api_token") String api_token, @PathVariable("group_id") String group_id, @RequestBody List<String> friendIds)  {
        System.out.println("Post update group " + group_id);
        for (String id : friendIds) {
            System.out.println(id);
        }
        OAuth2Operations oauth = new FacebookConnectionFactory(APP_ID, APP_SECRET).getOAuthOperations();
        String accesToken = oauth.authenticateClient().getAccessToken();
        Facebook facebook = new FacebookTemplate(api_token);
        String user_id = facebook.userOperations().getUserProfile().getId();
        Group group = groupRepository.findById(group_id);
        for (String id : friendIds) {
            if (!group.getMembers().contains(id))
                group.getMembers().add(id);
        }
        groupRepository.save(group);
        return null;
    }

    @RequestMapping(value = "/user/{api_token}/{group_id}/leave", method = RequestMethod.GET)
    public Void leaveGroup(@PathVariable("api_token") String api_token, @PathVariable("group_id") String group_id)  {
        System.out.println("Post leave group " + group_id);
        OAuth2Operations oauth = new FacebookConnectionFactory(APP_ID, APP_SECRET).getOAuthOperations();
        String accesToken = oauth.authenticateClient().getAccessToken();
        Facebook facebook = new FacebookTemplate(api_token);
        String user_id = facebook.userOperations().getUserProfile().getId();
        Group group = groupRepository.findById(group_id);
        group.getMembers().remove(user_id);
        groupRepository.save(group);
        return null;
    }

    @RequestMapping(value = "/user/{api_token}/{friend_id}/file", method = RequestMethod.POST)
    public Void uploadFile(@PathVariable("api_token") String api_token,
                           @PathVariable("friend_id") String friend_id,
                           @RequestPart MultipartFile file) {
        OAuth2Operations oauth = new FacebookConnectionFactory(APP_ID, APP_SECRET).getOAuthOperations();
        String accesToken = oauth.authenticateClient().getAccessToken();
        Facebook facebook = new FacebookTemplate(api_token);
        String user_id = facebook.userOperations().getUserProfile().getId();
        System.out.println("Method uploadFile " + file.getOriginalFilename());
        if (!file.isEmpty()) {
            try {
                File theFile = new File(this.getClass().getResource("").getPath() + "/uploads/" + user_id + "/" + friend_id + "/" + file.getOriginalFilename());
                File dir = theFile.getParentFile();
                if (!dir.exists())
                    dir.mkdirs();

                BufferedOutputStream stream = new BufferedOutputStream(
                        new FileOutputStream(theFile));
                FileCopyUtils.copy(file.getInputStream(), stream);
                stream.close();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @RequestMapping(value = "/user/{api_token}/{friend_id}/file/{filename:.+}", method = RequestMethod.GET)
    public void downloadFile(@PathVariable("api_token") String api_token,
                             @PathVariable("friend_id") String friend_id,
                             @PathVariable("filename") String filename,
                             HttpServletResponse response) {
        OAuth2Operations oauth = new FacebookConnectionFactory(APP_ID, APP_SECRET).getOAuthOperations();
        String accesToken = oauth.authenticateClient().getAccessToken();
        Facebook facebook = new FacebookTemplate(api_token);
        String user_id = facebook.userOperations().getUserProfile().getId();
        System.out.println("Method downloadFile " + filename);
        try {
            File theFile = new File(this.getClass().getResource("").getPath() + "/uploads/" + friend_id + "/" + user_id + "/" + filename);
            if (!theFile.exists()) {
                System.out.println("Failed " + theFile.getPath());
                System.out.println("Failed " + filename);
                return;
            }

            BufferedInputStream stream = new BufferedInputStream(
                    new FileInputStream(theFile));
            FileCopyUtils.copy(stream, response.getOutputStream());
            stream.close();

            response.flushBuffer();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
