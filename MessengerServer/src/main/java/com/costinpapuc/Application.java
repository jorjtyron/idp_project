package com.costinpapuc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.Arrays;


@SpringBootApplication
public class Application implements CommandLineRunner{
	@Autowired
	private GroupRepository groupRepository;

	@Autowired
	private MessageRepository messageRepository;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		groupRepository.deleteAll();
		messageRepository.deleteAll();
		String id1=  "104330836636826";
		String id2 = "140838086311890";
		String id3=  "111078235960431";
		String id4 = "122534188145784";
		messageRepository.save(new Message(id1, id2, System.currentTimeMillis(), "Salut"));
		messageRepository.save(new Message(id1, id2, System.currentTimeMillis(), "Ce faci?"));
		messageRepository.save(new Message(id2, id1, System.currentTimeMillis(), "Buna. Bine, tu?"));

		//Thread.sleep(26000);
		messageRepository.save(new Message(id1, id2, System.currentTimeMillis(), "Haha"));
		messageRepository.save(new Message(id1, id2, System.currentTimeMillis(), "Noua"));

		String[] s = {"140838086311890", "104330836636826", "137681999961637"};
		String[] s2 = {"140838086311890", "104330836636826", "111078235960431", "122534188145784"};

		groupRepository.save(new Group("Family", Arrays.asList(s)));
		groupRepository.save(new Group("School", Arrays.asList(s2)));

		Group group = groupRepository.findByName("School");

		messageRepository.save(new Message(id1, group.getId(), System.currentTimeMillis(), "Salut"));
		messageRepository.save(new Message(id3, group.getId(), System.currentTimeMillis(), "Salutare"));
		messageRepository.save(new Message(id2, group.getId(), System.currentTimeMillis(), "IonVlasaia:BUNA ZIUA"));
		messageRepository.save(new Message(id4, group.getId(), System.currentTimeMillis(), "Aloha"));
		messageRepository.save(new Message(id1, group.getId(), System.currentTimeMillis(), "Ce faceti oameni buni?"));

//		for (Group group : groupRepository.findAll()) {
//			System.out.println(group);
//		}

	}
}
